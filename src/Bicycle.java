// Nicoleta Sarghi 2039804
public class Bicycle {
    private String manufacturer;
    int numberGears;
    double maxSpeed;
    public Bicycle(String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }
    // Get methods for all three private fields
    public String getManufacturer() {
        return manufacturer;
    }
    public double getMaxSpeed() {
        return maxSpeed;
    }
    public int getNumberGears() {
        return numberGears;
    }
    // Tostring method for printing
    @Override
    public String toString() {
        return "{ Manufacturer: "+manufacturer+", numberGears: "+numberGears+", maxSpeed: "+maxSpeed+" }";
    }
}
