// Nicoleta Sarghi 2039804
public class BikeStore {
    public static void main(String[] args) {
        Bicycle[] bikes = createBikes();
        displayBikes(bikes);    
    }
    // Creates 4 new bikes for the bike store
    public static Bicycle[] createBikes(){
        Bicycle[] bikes = new Bicycle[4];
        bikes[0] = new Bicycle("Gucci",5,16);
        bikes[1] = new Bicycle("Garfield and Co.",10,80);        
        bikes[2] = new Bicycle("Nintendo?",2,30);        
        bikes[3] = new Bicycle("Papa Johns",1,5); 
        return bikes;
    }
    // Prints out all bike data
    public static void displayBikes(Bicycle[] bikes){
        for(Bicycle bike : bikes){
            System.out.println(bike);
        }
    }
}
